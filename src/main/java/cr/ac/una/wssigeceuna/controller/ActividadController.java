/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wssigeceuna.controller;

import cr.ac.una.wssigeceuna.model.ActividadDto;
import cr.ac.una.wssigeceuna.service.ActividadService;
import cr.ac.una.wssigeceuna.util.CodigoRespuesta;
import cr.ac.una.wssigeceuna.util.Respuesta;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author JosueNG
 */
@Path("/ActividadController")
public class ActividadController {

    @EJB
    ActividadService actividadService;

    @Context
    SecurityContext securityContext;

    @GET
    public Response ping() {
        return Response
                .ok("ping")
                .build();
    }

    @GET
    @Path("/actividad/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getActividad(@PathParam("id") Long id) {
        try {
            Respuesta res = actividadService.getActividad(id);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok((ActividadDto) res.getResultado("Actividad")).build();
        } catch (Exception ex) {
            Logger.getLogger(ActividadController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo la actividad").build();
        }
    }

    @GET
    @Path("/actividades")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getActividades() {
        try {
            Respuesta res = actividadService.getActividades();
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok(new GenericEntity<List<ActividadDto>>((List<ActividadDto>) res.getResultado("Actividades")) {
            }).build();
        } catch (Exception ex) {
            Logger.getLogger(ActividadController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo las actividades").build();
        }
    }

    @PUT
    @Path("/actividad/{id}/{prioridad}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response cambiarPrioridad(@PathParam("id") Long id,@PathParam("prioridad") Long prioridad) {
        try {
            Respuesta respuesta = actividadService.cambiarPrioridad(id, prioridad);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok((ActividadDto) respuesta.getResultado("Actividad")).build();
        } catch (Exception ex) {
            Logger.getLogger(ActividadController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error cambiando la prioridad").build();
        }
    }

    @POST
    @Path("/actividad")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarActividad(ActividadDto actividad) {
        try {
            Respuesta respuesta = actividadService.guardarActividad(actividad);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok((ActividadDto) respuesta.getResultado("Actividad")).build();
        } catch (Exception ex) {
            Logger.getLogger(ActividadController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error guardando la actividad").build();
        }
    }

    @DELETE
    @Path("/actividad/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response eliminarActividad(@PathParam("id") Long id) {
        try {
            Respuesta respuesta = actividadService.eliminarActividad(id);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok().build();
        } catch (Exception ex) {
            Logger.getLogger(ActividadController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error eliminando la actividad").build();
        }
    }

}
