/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wssigeceuna.controller;

import cr.ac.una.wssigeceuna.model.SubActividadDto;
import cr.ac.una.wssigeceuna.service.SubActividadService;
import cr.ac.una.wssigeceuna.util.CodigoRespuesta;
import cr.ac.una.wssigeceuna.util.Respuesta;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author Ale
 */
@Path("/SubActividadController")
public class SubActividadController {
    
    @EJB
    SubActividadService subActividadService;
    
    @Context
    SecurityContext securityContext;
    
    @GET
    public Response ping(){
        return Response
                .ok("ping")
                .build();
    }
    
    @GET
    @Path("/subActividad/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON) 
    public Response getSubActividad(@PathParam("id") Long id) {
        try {
            Respuesta res = subActividadService.getSubActividad(id);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok((SubActividadDto)res.getResultado("SubActividad")).build();
        } catch (Exception ex) {
            Logger.getLogger(SubActividadController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo la subActividad").build();
        }
    }
    
    @GET
    @Path("/subActividades")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getSubActividades() {
        try {
            Respuesta res = subActividadService.getSubActividades();
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok(new GenericEntity<List<SubActividadDto>>((List<SubActividadDto>) res.getResultado("SubActividades")) {
            }).build();
        } catch (Exception ex) {
            Logger.getLogger(SubActividadController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo las subactividades").build();
        }
    }
    
    @PUT
    @Path("/subActividad/{id}/{prioridad}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response cambiarPrioridad(@PathParam("id") Long id,@PathParam("prioridad") Long prioridad) {
        try {
            Respuesta respuesta = subActividadService.cambiarPrioridad(id, prioridad);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok((SubActividadDto) respuesta.getResultado("SubActividad")).build();
        } catch (Exception ex) {
            Logger.getLogger(SubActividadController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error cambiando la prioridad").build();
        }
    }

    @POST
    @Path("/subActividad")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarSubActividad(SubActividadDto subActividad) {
        try {
            Respuesta respuesta = subActividadService.guardarSubActividad(subActividad);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok((SubActividadDto)respuesta.getResultado("SubActividad")).build();
        } catch (Exception ex) {
            Logger.getLogger(SubActividadController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error guardando la subactividad").build();
        }
    }
    
    @DELETE
    @Path("/subActividad/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response eliminarSubActividad(@PathParam("id") Long id) {
        try {
            Respuesta respuesta = subActividadService.eliminarSubActividad(id);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok().build();
        } catch (Exception ex) {
            Logger.getLogger(SubActividadController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error eliminando la subactividad").build();
        }
    }
    
}

