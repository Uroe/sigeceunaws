/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wssigeceuna.controller;

import cr.ac.una.wssigeceuna.model.GestionDto;
import cr.ac.una.wssigeceuna.service.GestionService;
import cr.ac.una.wssigeceuna.util.CodigoRespuesta;
import cr.ac.una.wssigeceuna.util.Respuesta;
import java.time.LocalDate;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Lesmi
 */
@Path("/GestionController")
public class GestionController {

    @EJB
    private GestionService service;

    @GET
    @Path("/gestion/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGestion(@PathParam("id") Long id) {
        try {
            Respuesta res = service.getGestion(id);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok((GestionDto) res.getResultado("Gestion")).build();
        } catch (Exception ex) {
            Logger.getLogger(GestionController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo la gestion").build();
        }
    }

    @GET
    @Path("/gestiones")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getGestiones() {
        try {
            Respuesta res = service.getGestiones();
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok(new GenericEntity<List<GestionDto>>((List<GestionDto>) res.getResultado("Gestiones")) {
            }).build();
        } catch (Exception ex) {
            Logger.getLogger(GestionController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo las gestiones").build();
        }
    }

    @GET
    @Path("/gestionesmes/{mes}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getGestionesMes(@PathParam("mes") int mes) {
        try {
            Respuesta res = service.getGestionesMes(mes);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok(new GenericEntity<List<GestionDto>>((List<GestionDto>) res.getResultado("GestionesMes")) {
            }).build();
        } catch (Exception ex) {
            Logger.getLogger(GestionController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo las gestiones").build();
        }
    }

    @GET
    @Path("/gestionessoli/{idSoli}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getGestionesSolicitadas(@PathParam("idSoli") Long idSoli) {
        try {
            Respuesta res = service.getGestionesSolicitadas(idSoli);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok(new GenericEntity<List<GestionDto>>((List<GestionDto>) res.getResultado("Gestiones")) {
            }).build();
        } catch (Exception ex) {
            Logger.getLogger(GestionController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo las gestiones").build();
        }
    }

    @GET
    @Path("/gestionessolifin/{idSoli}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getGestionesSoliFin(@PathParam("idSoli") Long idSoli) {
        try {
            Respuesta res = service.getGestionesSoliFin(idSoli);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok(new GenericEntity<List<GestionDto>>((List<GestionDto>) res.getResultado("Gestiones")) {
            }).build();
        } catch (Exception ex) {
            Logger.getLogger(GestionController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo las gestiones").build();
        }
    }

    @GET
    @Path("/gestionesasig/{idAsig}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getGestionesAsignadas(@PathParam("idAsig") Long idAsig) {
        try {
            Respuesta res = service.getGestionesAsignadas(idAsig);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok(new GenericEntity<List<GestionDto>>((List<GestionDto>) res.getResultado("Gestiones")) {
            }).build();
        } catch (Exception ex) {
            Logger.getLogger(GestionController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo las gestiones").build();
        }
    }

    @POST
    @Path("/gestion")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarGestion(GestionDto gestionDto) {
        try {
            Respuesta respuesta = service.guardarGestion(gestionDto);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok((GestionDto) respuesta.getResultado("Gestion")).build();
        } catch (Exception ex) {
            Logger.getLogger(GestionController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error guardando la gestion").build();
        }
    }

    @PUT
    @Path("/gestion/{id}/{estado}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response cambiarEstado(@PathParam("id") Long id, @PathParam("estado") String estado) {
        try {
            Respuesta respuesta = service.cambiarEstado(id, estado);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok((GestionDto) respuesta.getResultado("Gestion")).build();
        } catch (Exception ex) {
            Logger.getLogger(GestionController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error cambiando el estado").build();
        }
    }

    @PUT
    @Path("/gestion/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response asignarFsolucion(@PathParam("id") Long id) {
        try {
            Respuesta respuesta = service.asignarFsolucion(id);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok((GestionDto) respuesta.getResultado("Gestion")).build();
        } catch (Exception ex) {
            Logger.getLogger(GestionController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error asignando la fecha").build();
        }
    }

    @DELETE
    @Path("/gestion/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response eliminarGestion(@PathParam("id") Long id) {
        try {
            Respuesta respuesta = service.eliminarGestion(id);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok().build();
        } catch (Exception ex) {
            Logger.getLogger(GestionController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error eliminando la gestion").build();
        }
    }

    @GET
    @Path("/reporte/{id}/{fecha1}/{fecha2}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getReporte(@PathParam("id") Long id, @PathParam("fecha1") String fecha1, @PathParam("fecha2") String fecha2) {
        try {
            Respuesta res = service.getReporte(id, fecha1, fecha2);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok((String) res.getResultado("Reporte")).build();
        } catch (Exception ex) {
            Logger.getLogger(GestionController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo el reporte").build();
        }
    }

    @GET
    @Path("/reporteareas")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getReporteAreas() {
        try {
            Respuesta res = service.getReporteAreas();
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok((String) res.getResultado("Reporte")).build();
        } catch (Exception ex) {
            Logger.getLogger(GestionController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo el reporte").build();
        }
    }
    
    @GET
    @Path("/reporteempleados/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getReporteEmpleados(@PathParam("id") Long id) {
        try {
            Respuesta res = service.getReporteEmpleados(id);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok((String) res.getResultado("Reporte")).build();
        } catch (Exception ex) {
            Logger.getLogger(GestionController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo el reporte").build();
        }
    }
}
