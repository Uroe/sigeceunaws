/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wssigeceuna.controller;

import cr.ac.una.wssigeceuna.model.AprobadorDto;
import cr.ac.una.wssigeceuna.service.AprobadorService;
import cr.ac.una.wssigeceuna.util.CodigoRespuesta;
import cr.ac.una.wssigeceuna.util.Respuesta;
import cr.ac.una.wssigeceuna.util.Secure;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author JosueNG
 */
@Path("/AprobadorController")
public class AprobadorController {

    @EJB
    AprobadorService aprobadorService;

    @GET
    @Path("/aprobador/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getAprobador(@PathParam("id") Long id) {
        try {
            Respuesta res = aprobadorService.getAprobador(id);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok((AprobadorDto) res.getResultado("Aprobador")).build();
        } catch (Exception ex) {
            Logger.getLogger(AprobadorController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo el aprobador").build();
        }
    }

    @GET
    @Path("/aprobadores")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getAprobadores() {
        try {
            Respuesta res = aprobadorService.getAprobadores();
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok(new GenericEntity<List<AprobadorDto>>((List<AprobadorDto>) res.getResultado("Aprobadores")) {
            }).build();
        } catch (Exception ex) {
            Logger.getLogger(AprobadorController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo los aprobadores").build();
        }
    }

    @POST
    @Path("/aprobador")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarAprobador(AprobadorDto aprobador) {
        try {
            Respuesta respuesta = aprobadorService.guardarAprobador(aprobador);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok((AprobadorDto) respuesta.getResultado("Aprobador")).build();
        } catch (Exception ex) {
            Logger.getLogger(AprobadorController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error guardando el aprobador").build();
        }
    }

    @DELETE
    @Path("/aprobador/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response eliminarAprobador(@PathParam("id") Long id) {
        try {
            Respuesta respuesta = aprobadorService.eliminarAprobador(id);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok().build();
        } catch (Exception ex) {
            Logger.getLogger(AprobadorController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error eliminando el aprobador").build();
        }
    }

}
