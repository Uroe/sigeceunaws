/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wssigeceuna.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 *
 * @author Lesmi
 */
@Entity
@Table(name = "SIGECE_APROBADORES", schema = "SIGECEUNA")
@NamedQueries({
    @NamedQuery(name = "Aprobador.findAll", query = "SELECT a FROM Aprobador a"),
    @NamedQuery(name = "Aprobador.findByAproId", query = "SELECT a FROM Aprobador a WHERE a.aproId = :aproId")
})
public class Aprobador implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "APRO_ID")
    private Long aproId;
    @Basic(optional = false)
    @Column(name = "APRO_COMENTARIO")
    private String aproComentario;
    @Column(name = "APRO_APROBACION")
    private String aproAprobacion;
    @Version
    @Column(name = "APRO_VERSION")
    private Long aproVersion;
    @ManyToMany(mappedBy = "aprobadores", fetch = FetchType.LAZY)
    private List<Gestion> gestiones;

    public Aprobador() {
    }

    public Aprobador(Long aproId) {
        this.aproId = aproId;
    }

    public Aprobador(Long aproId, String aproComentario, String aproAprobacion) {
        this.aproId = aproId;
        this.aproComentario = aproComentario;
        this.aproAprobacion = aproAprobacion;
    }
    
    public Aprobador(AprobadorDto aprobador){
        this.aproId = aprobador.getId();
        actualizarAprobador(aprobador);
    } 
    
    public void  actualizarAprobador(AprobadorDto aprobador){
        this.aproComentario = aprobador.getComentario();
        this.aproAprobacion = aprobador.getAprobacion();
    }

    public Long getAproId() {
        return aproId;
    }

    public void setAproId(Long aproId) {
        this.aproId = aproId;
    }

    public String getAproComentario() {
        return aproComentario;
    }

    public void setAproComentario(String aproComentario) {
        this.aproComentario = aproComentario;
    }

    public String getAproAprobacion() {
        return aproAprobacion;
    }

    public void setAproAprobacion(String aproAprobacion) {
        this.aproAprobacion = aproAprobacion;
    }

    public Long getAproVersion() {
        return aproVersion;
    }

    public void setAproVersion(Long aproVersion) {
        this.aproVersion = aproVersion;
    }

    public List<Gestion> getGestiones() {
        return gestiones;
    }

    public void setGestiones(List<Gestion> gestiones) {
        this.gestiones = gestiones;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (aproId != null ? aproId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Aprobador)) {
            return false;
        }
        Aprobador other = (Aprobador) object;
        if ((this.aproId == null && other.aproId != null) || (this.aproId != null && !this.aproId.equals(other.aproId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.ac.una.wssigeceuna.model.Aprobador[ aproId=" + aproId + " ]";
    }

}
