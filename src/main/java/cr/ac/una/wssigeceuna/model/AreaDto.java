/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wssigeceuna.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author JosueNG
 */
public class AreaDto {

    private Long id;
    private String nombre;
    private String estado;
    private byte[] logo;
    private Boolean modificado;
    private List<EmpleadoDto> empleados;
    private List<EmpleadoDto> empleadosEliminados;
    private List<ActividadDto> actividades;

    public AreaDto() {
        this.modificado = false;
        actividades = new ArrayList<>();
        empleados = new ArrayList<>();
        empleadosEliminados = new ArrayList<>();
    }

    public AreaDto(Area area) {
        this();
        this.id = area.getAreId();
        this.nombre = area.getAreNombre();
        this.estado = area.getAreEstado();
        this.logo = area.getAreLogo();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    public List<ActividadDto> getActividades() {
        return actividades;
    }

    public void setActividades(List<ActividadDto> actividades) {
        this.actividades = actividades;
    }

    public List<EmpleadoDto> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(List<EmpleadoDto> empleados) {
        this.empleados = empleados;
    }

    @Override
    public String toString() {
        return "AreaDto{" + "id=" + id + ", nombre=" + nombre + ", estado=" + estado + '}';
    }

    public List<EmpleadoDto> getEmpleadosEliminados() {
        return empleadosEliminados;
    }

    public void setEmpleadosEliminados(List<EmpleadoDto> empleadosEliminados) {
        this.empleadosEliminados = empleadosEliminados;
    }

}
