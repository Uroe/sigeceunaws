/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wssigeceuna.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author JosueNG
 */
public class EmpleadoDto {
 
    private Long id;
    private Boolean modificado;
    private List<AreaDto> areas;

    public EmpleadoDto() {
        this.modificado = false;
        areas = new ArrayList<>(); 
    }
    
    public EmpleadoDto(Empleado empleado) {
        this();
        this.id = empleado.getId();
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

    public List<AreaDto> getAreas() {
        return areas;
    }

    public void setAreas(List<AreaDto> areas) {
        this.areas = areas;
    }
    
    @Override
    public String toString() {
        return "EmpleadoDto{" + "id=" + id + '}';
    }
    
}
