/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wssigeceuna.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 *
 * @author Lesmi
 */
@Entity
@Table(name = "SIGECE_ACTIVIDADES", schema = "SIGECEUNA")
@NamedQueries({
    @NamedQuery(name = "Actividad.findAll", query = "SELECT a FROM Actividad a"),
    @NamedQuery(name = "Actividad.findByActId", query = "SELECT a FROM Actividad a WHERE a.actId = :actId")})
public class Actividad implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "SIGECE_ACTIVIDADES_ACT_ID_GENERATOR", sequenceName = "SIGECEUNA.SIGECE_ACTIVIDADES_SEQ01", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SIGECE_ACTIVIDADES_ACT_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "ACT_ID")
    private Long actId;
    @Basic(optional = false)
    @Column(name = "ACT_NOMBRE")
    private String actNombre;
    @Column(name = "ACT_PRIORIDAD")
    private Long actPrioridad;
    @Version
    @Column(name = "ACT_VERSION")
    private Long actVersion;
    @OneToMany(mappedBy = "actividad")
    private List<Subactividad> subActividades;
    @JoinColumn(name = "ACT_IDARE", referencedColumnName = "ARE_ID")
    @ManyToOne
    private Area area;
    @OneToMany(mappedBy = "actividad")
    private List<Gestion> gestiones;

    public Actividad() {
    }

    public Actividad(Long actId) {
        this.actId = actId;
    }

    public Actividad(Long actId, String actNombre) {
        this.actId = actId;
        this.actNombre = actNombre;
    }

    public Actividad(ActividadDto actividad) {
        this.actId = actividad.getId();
        actualizarActividad(actividad);
    }

    public void actualizarActividad(ActividadDto actividad) {
        this.actNombre = actividad.getNombre();
        this.actPrioridad = actividad.getPrioridad();
    }

    public Long getActId() {
        return actId;
    }

    public void setActId(Long actId) {
        this.actId = actId;
    }

    public String getActNombre() {
        return actNombre;
    }

    public void setActNombre(String actNombre) {
        this.actNombre = actNombre;
    }

    public Long getActVersion() {
        return actVersion;
    }

    public void setActVersion(Long actVersion) {
        this.actVersion = actVersion;
    }

    public List<Subactividad> getSubActividades() {
        return subActividades;
    }

    public void setSubActividades(List<Subactividad> subActividades) {
        this.subActividades = subActividades;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public List<Gestion> getGestiones() {
        return gestiones;
    }

    public void setGestiones(List<Gestion> gestiones) {
        this.gestiones = gestiones;
    }

    public Long getActPrioridad() {
        return actPrioridad;
    }

    public void setActPrioridad(Long actPrioridad) {
        this.actPrioridad = actPrioridad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (actId != null ? actId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Actividad)) {
            return false;
        }
        Actividad other = (Actividad) object;
        if ((this.actId == null && other.actId != null) || (this.actId != null && !this.actId.equals(other.actId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.ac.una.wssigeceuna.model.Actividad[ actId=" + actId + " ]";
    }

}
