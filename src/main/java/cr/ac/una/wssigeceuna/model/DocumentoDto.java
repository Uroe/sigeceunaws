/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wssigeceuna.model;

/**
 *
 * @author Ale
 */
public class DocumentoDto {
    
    private Long docId;
    private byte[] docArchivo;
    private Boolean modificado;
    private GestionDto gestion;
    
    public DocumentoDto() {
        this.modificado = false;
        gestion = new GestionDto();
    }

    public DocumentoDto(Documento documento) {
        this();
        this.docId = documento.getId();
        this.docArchivo = documento.getDocArchivo();
    }

    public Long getDocId() {
        return docId;
    }

    public void setDocId(Long docId) {
        this.docId = docId;
    }

    public byte[] getDocArchivo() {
        return docArchivo;
    }

    public void setDocArchivo(byte[] docArchivo) {
        this.docArchivo = docArchivo;
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }
    
    public GestionDto getGestion() {
        return gestion;
    }

    public void setGestion(GestionDto gestion) {
        this.gestion = gestion;
    }

    @Override
    public String toString() {
        return "DocumentoDto{" + "docId=" + docId + ", docUrl=" + docArchivo + ", modificado=" + modificado + '}';
    }
    
}
