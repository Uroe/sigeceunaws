/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wssigeceuna.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lesmi
 */
public class GestionDto {

    private Long gesId;
    private Long gesSolicitante;
    private Long gesPasignada;
    private LocalDate gesFcreacion;
    private LocalDate gesFvencimiento;
    private LocalDate gesFsolucion;
    private String gesDescripcion;
    private String gesAsunto;
    private String gesEstado;
    private Long gesVersion;
    private Boolean modificado;

    private List<AprobadorDto> aprobadores;
    private List<AprobadorDto> aprobadoresElim;
    private List<DocumentoDto> documentos;
    private List<SeguimientoDto> seguimientos;
    private ActividadDto actividad;
    private SubActividadDto subActividad;

    public GestionDto() {
        this.modificado = false;
        aprobadores = new ArrayList<>();
        aprobadoresElim = new ArrayList<>();
        documentos = new ArrayList<>();
        seguimientos = new ArrayList<>();
        actividad = new ActividadDto();
        subActividad = new SubActividadDto();
    }

    public GestionDto(Gestion gestion) {
        this();
        this.gesId = gestion.getGesId();
        this.gesSolicitante = gestion.getGesSolicitante();
        this.gesPasignada = gestion.getGesPasignada();
        this.gesAsunto = gestion.getGesAsunto();
        this.gesDescripcion = gestion.getGesDescripcion();
        this.gesFcreacion = gestion.getGesFcreacion();
        this.gesVersion = gestion.getGesVersion();
        this.gesFsolucion = gestion.getGesFsolucion();
        if (gestion.getGesFvencimiento() != null) {
            this.gesFvencimiento = gestion.getGesFvencimiento();
        } else {
            this.gesFvencimiento = null;
        }
        this.gesEstado = gestion.getGesEstado();
    }

    public Long getGesId() {
        return gesId;
    }

    public void setGesId(Long gesId) {
        this.gesId = gesId;
    }

    public Long getGesSolicitante() {
        return gesSolicitante;
    }

    public void setGesSolicitante(Long gesSolicitante) {
        this.gesSolicitante = gesSolicitante;
    }

    public Long getGesPasignada() {
        return gesPasignada;
    }

    public void setGesPasignada(Long gesPasignada) {
        this.gesPasignada = gesPasignada;
    }

    public LocalDate getGesFcreacion() {
        return gesFcreacion;
    }

    public void setGesFcreacion(LocalDate gesFcreacion) {
        this.gesFcreacion = gesFcreacion;
    }

    public LocalDate getGesFvencimiento() {
        return gesFvencimiento;
    }

    public void setGesFvencimiento(LocalDate gesFvencimiento) {
        this.gesFvencimiento = gesFvencimiento;
    }

    public String getGesDescripcion() {
        return gesDescripcion;
    }

    public void setGesDescripcion(String gesDescripcion) {
        this.gesDescripcion = gesDescripcion;
    }

    public String getGesAsunto() {
        return gesAsunto;
    }

    public void setGesAsunto(String gesAsunto) {
        this.gesAsunto = gesAsunto;
    }

    public String getGesEstado() {
        return gesEstado;
    }

    public void setGesEstado(String gesEstado) {
        this.gesEstado = gesEstado;
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

    public List<AprobadorDto> getAprobadores() {
        return aprobadores;
    }

    public void setAprobadores(List<AprobadorDto> aprobadores) {
        this.aprobadores = aprobadores;
    }

    public List<DocumentoDto> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<DocumentoDto> documentos) {
        this.documentos = documentos;
    }

    public List<SeguimientoDto> getSeguimientos() {
        return seguimientos;
    }

    public void setSeguimientos(List<SeguimientoDto> seguimientos) {
        this.seguimientos = seguimientos;
    }

    public ActividadDto getActividad() {
        return actividad;
    }

    public void setActividad(ActividadDto actividad) {
        this.actividad = actividad;
    }

    public SubActividadDto getSubActividad() {
        return subActividad;
    }

    public void setSubActividad(SubActividadDto subActividad) {
        this.subActividad = subActividad;
    }

    public Long getGesVersion() {
        return gesVersion;
    }

    public void setGesVersion(Long gesVersion) {
        this.gesVersion = gesVersion;
    }

    public List<AprobadorDto> getAprobadoresElim() {
        return aprobadoresElim;
    }

    public void setAprobadoresElim(List<AprobadorDto> aprobadoresElim) {
        this.aprobadoresElim = aprobadoresElim;
    }

    public LocalDate getGesFsolucion() {
        return gesFsolucion;
    }

    public void setGesFsolucion(LocalDate gesFsolucion) {
        this.gesFsolucion = gesFsolucion;
    }

}
