/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wssigeceuna.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 *
 * @author Lesmi
 */
@Entity
@Table(name = "SIGECE_SUBACTIVIDADES", schema = "SIGECEUNA")
@NamedQueries({
    @NamedQuery(name = "Subactividad.findAll", query = "SELECT s FROM Subactividad s"),
    @NamedQuery(name = "Subactividad.findBySubactId", query = "SELECT s FROM Subactividad s WHERE s.id = :id")
})
public class Subactividad implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "SIGECE_SUBACTIVIDADES_SUBACT_ID_GENERATOR", sequenceName = "SIGECEUNA.SIGECE_SUBACTIVIDADES_SEQ01", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SIGECE_SUBACTIVIDADES_SUBACT_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "SUBACT_ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "SUBACT_NOMBRE")
    private String nombre;
    @Column(name = "SUBACT_PRIORIDAD")
    private Long prioridad;
    @Version
    @Column(name = "SUBACT_VERSION")
    private Long version;
    @JoinColumn(name = "SUBACT_IDACT", referencedColumnName = "ACT_ID")
    @ManyToOne
    private Actividad actividad;
    @OneToMany(mappedBy = "subActividad")
    private List<Gestion> gestiones;

    public Subactividad() {
    }

    public Subactividad(Long subactId) {
        this.id = subactId;
    }

    public Subactividad(Long subactId, String subactNombre) {
        this.id = subactId;
        this.nombre = subactNombre;
    }


    public Subactividad(SubActividadDto subActividad) {
        this.id = subActividad.getId();
        actualizarSubActividad(subActividad);
    }

    public void actualizarSubActividad(SubActividadDto subActividad) {
        this.nombre = subActividad.getNombre();
        this.prioridad = subActividad.getPrioridad();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long subactId) {
        this.id = subactId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String subactNombre) {
        this.nombre = subactNombre;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long subactVersion) {
        this.version = subactVersion;
    }

    public Actividad getActividad() {
        return actividad;
    }

    public void setActividad(Actividad actividad) {
        this.actividad = actividad;
    }

    public List<Gestion> getGestiones() {
        return gestiones;
    }

    public void setGestiones(List<Gestion> gestiones) {
        this.gestiones = gestiones;
    }

    public Long getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(Long prioridad) {
        this.prioridad = prioridad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Subactividad)) {
            return false;
        }
        Subactividad other = (Subactividad) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.ac.una.wssigeceuna.model.Subactividad[ subactId=" + id + " ]";
    }

}
