/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wssigeceuna.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 *
 * @author Lesmi
 */
@Entity
@Table(name = "SIGECE_AREAS", schema = "SIGECEUNA")
@NamedQueries({
    @NamedQuery(name = "Area.findAll", query = "SELECT a FROM Area a"),
    @NamedQuery(name = "Area.findByAreId", query = "SELECT a FROM Area a WHERE a.areId = :areId")})
public class Area implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "SIGECE_AREAS_ARE_ID_GENERATOR", sequenceName = "SIGECEUNA.SIGECE_AREAS_SEQ01", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SIGECE_AREAS_ARE_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "ARE_ID")
    private Long areId;
    @Basic(optional = false)
    @Column(name = "ARE_NOMBRE")
    private String areNombre;
    @Basic(optional = false)
    @Column(name = "ARE_ESTADO")
    private String areEstado;
    @Lob
    @Column(name = "ARE_LOGO")
    private byte[] areLogo;
    @Version
    @Column(name = "ARE_VERSION")
    private Long areVersion;
    @OneToMany(mappedBy = "area")
    private List<Actividad> actividades;
    @JoinTable(name = "SIGECE_AREAEMPLEADOS", joinColumns = {
        @JoinColumn(name = "EXA_IDARE", referencedColumnName = "ARE_ID")}, inverseJoinColumns = {
        @JoinColumn(name = "EXA_IDEMP", referencedColumnName = "EMP_ID")})
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Empleado> empleados;

    public Area() {
    }

    public Area(Long areId) {
        this.areId = areId;
    }

    public Area(Long areId, String areNombre, String areEstado) {
        this.areId = areId;
        this.areNombre = areNombre;
        this.areEstado = areEstado;
    }

    public Area(AreaDto area) {
        this.areId = area.getId();
        actualizarArea(area);
    }

    public void actualizarArea(AreaDto area) {
        this.areNombre = area.getNombre();
        this.areEstado = area.getEstado();
        this.areLogo = area.getLogo();
    }

    public Long getAreId() {
        return areId;
    }

    public void setAreId(Long areId) {
        this.areId = areId;
    }

    public String getAreNombre() {
        return areNombre;
    }

    public void setAreNombre(String areNombre) {
        this.areNombre = areNombre;
    }

    public String getAreEstado() {
        return areEstado;
    }

    public void setAreEstado(String areEstado) {
        this.areEstado = areEstado;
    }

    public Long getAreVersion() {
        return areVersion;
    }

    public void setAreVersion(Long areVersion) {
        this.areVersion = areVersion;
    }

    public List<Actividad> getActividades() {
        return actividades;
    }

    public void setActividades(List<Actividad> actividades) {
        this.actividades = actividades;
    }

    public byte[] getAreLogo() {
        return areLogo;
    }

    public void setAreLogo(byte[] areLogo) {
        this.areLogo = areLogo;
    }

    public List<Empleado> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(List<Empleado> empleados) {
        this.empleados = empleados;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (areId != null ? areId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Area)) {
            return false;
        }
        Area other = (Area) object;
        if ((this.areId == null && other.areId != null) || (this.areId != null && !this.areId.equals(other.areId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.ac.una.wssigeceuna.model.Area[ areId=" + areId + " ]";
    }

}
