/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wssigeceuna.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author JosueNG
 */
public class ActividadDto {

    private Long id;
    private String nombre;
    private Long prioridad;
    private Boolean modificado;
    private AreaDto area;
    private List<GestionDto> gestiones;
    private List<SubActividadDto> subactividades;

    public ActividadDto() {
        this.modificado = false;
        area = new AreaDto();
        gestiones = new ArrayList<>();
        subactividades = new ArrayList<>();
    }

    public ActividadDto(Actividad actividad) {
        this();
        this.id = actividad.getActId();
        this.nombre = actividad.getActNombre();
        this.prioridad = actividad.getActPrioridad();
    }

    public Long getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(Long prioridad) {
        this.prioridad = prioridad;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

    public AreaDto getArea() {
        return area;
    }

    public void setArea(AreaDto area) {
        this.area = area;
    }

    public List<GestionDto> getGestiones() {
        return gestiones;
    }

    public void setGestiones(List<GestionDto> gestiones) {
        this.gestiones = gestiones;
    }

    public List<SubActividadDto> getSubactividades() {
        return subactividades;
    }

    public void setSubactividades(List<SubActividadDto> subactividades) {
        this.subactividades = subactividades;
    }

    @Override
    public String toString() {
        return "ActividadDto{" + "id=" + id + ", nombre=" + nombre + '}';
    }

}
