/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wssigeceuna.model;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 *
 * @author Lesmi
 */
@Entity
@Table(name = "SIGECE_SEGUIMIENTOS", schema = "SIGECEUNA")
@NamedQueries({
    @NamedQuery(name = "Seguimiento.findAll", query = "SELECT s FROM Seguimiento s"),
    @NamedQuery(name = "Seguimiento.findBySegId", query = "SELECT s FROM Seguimiento s WHERE s.id = :id"),
    @NamedQuery(name = "Seguimiento.findBySegIdusuario", query = "SELECT s FROM Seguimiento s WHERE s.idusuario = :idusuario")})
public class Seguimiento implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "SIGECE_SEGUIMIENTOS_SEG_ID_GENERATOR", sequenceName = "SIGECEUNA.SIGECE_SEGUIMIENTOS_SEQ01", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SIGECE_SEGUIMIENTOS_SEG_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "SEG_ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "SEG_FSEGUIMIENTO")
    private LocalDate fseguimiento;
    @Basic(optional = false)
    @Column(name = "SEG_IDUSUARIO")
    private Long idusuario;
    @Basic(optional = false)
    @Column(name = "SEG_DETALLE")
    private String detalle;
    @Version
    @Column(name = "SEG_VERSION")
    private Long version;
    @JoinColumn(name = "SEG_IDGES", referencedColumnName = "GES_ID")
    @ManyToOne
    private Gestion gestion;

    public Seguimiento() {
    }

    public Seguimiento(Long segId) {
        this.id = segId;
    }

    public Seguimiento(Long segId, LocalDate segFseguimiento, Long segIdusuario) {
        this.id = segId;
        this.fseguimiento = segFseguimiento;
        this.idusuario = segIdusuario;
    }
    
    public Seguimiento(SeguimientoDto seguimiento) {
        this.id = seguimiento.getSegId();
        actualizarSeguimiento(seguimiento);
    }
    
     public void actualizarSeguimiento(SeguimientoDto seguimiento) {
        this.fseguimiento = seguimiento.getSegFseguimiento();
        this.idusuario = seguimiento.getSegIdusuario();
        this.detalle = seguimiento.getSegDetalle();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long segId) {
        this.id = segId;
    }

    public LocalDate getFseguimiento() {
        return fseguimiento;
    }

    public void setFseguimiento(LocalDate segFseguimiento) {
        this.fseguimiento = segFseguimiento;
    }

    public Long getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(Long segIdusuario) {
        this.idusuario = segIdusuario;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String segDetalle) {
        this.detalle = segDetalle;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long segVersion) {
        this.version = segVersion;
    }

    public Gestion getGestion() {
        return gestion;
    }

    public void setGestion(Gestion gestion) {
        this.gestion = gestion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Seguimiento)) {
            return false;
        }
        Seguimiento other = (Seguimiento) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.ac.una.wssigeceuna.model.Seguimiento[ segId=" + id + " ]";
    }

}
