/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wssigeceuna.model;

import java.time.LocalDate;

/**
 *
 * @author Ale
 */
public class SeguimientoDto {
    
    private Long segId;
    private LocalDate segFseguimiento;
    private Long segIdusuario;
    private String segDetalle;
    private Boolean modificado;
    private GestionDto gestion;

    public SeguimientoDto() {
        this.modificado = false;
        gestion = new GestionDto();
    }
    
    public SeguimientoDto(Seguimiento seguimiento) {
        this();
        this.segId = seguimiento.getId();
        this.segFseguimiento = seguimiento.getFseguimiento();
        this.segIdusuario = seguimiento.getIdusuario();
        this.segDetalle = seguimiento.getDetalle();
    }

    public Long getSegId() {
        return segId;
    }

    public void setSegId(Long segId) {
        this.segId = segId;
    }

    public LocalDate getSegFseguimiento() {
        return segFseguimiento;
    }

    public void setSegFseguimiento(LocalDate segFseguimiento) {
        this.segFseguimiento = segFseguimiento;
    }

    public Long getSegIdusuario() {
        return segIdusuario;
    }

    public void setSegIdusuario(Long segIdusuario) {
        this.segIdusuario = segIdusuario;
    }

    public String getSegDetalle() {
        return segDetalle;
    }

    public void setSegDetalle(String segDetalle) {
        this.segDetalle = segDetalle;
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }
    
    public GestionDto getGestion() {
        return gestion;
    }

    public void setGestion(GestionDto gestion) {
        this.gestion = gestion;
    }

    @Override
    public String toString() {
        return "SeguimientoDto{" + "segId=" + segId + ", segFseguimiento=" + segFseguimiento + ", segIdusuario=" + segIdusuario + ", segDetalle=" + segDetalle + ", modificado=" + modificado + '}';
    }
    
}
