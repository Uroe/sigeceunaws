/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wssigeceuna.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ale
 */
public class SubActividadDto {

    private Long id;
    private String nombre;
    private Long prioridad;
    private Boolean modificado;
    private ActividadDto actividad;
    List<Gestion> gestiones;

    public SubActividadDto() {
        this.modificado = false;
        actividad = new ActividadDto();
        gestiones = new ArrayList<>();
    }

    public SubActividadDto(Subactividad subActividad) {
        this();
        this.id = subActividad.getId();
        this.nombre = subActividad.getNombre();
        this.prioridad = subActividad.getPrioridad();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

    public ActividadDto getActividad() {
        return actividad;
    }

    public void setActividad(ActividadDto actividad) {
        this.actividad = actividad;
    }

    public List<Gestion> getGestiones() {
        return gestiones;
    }

    public void setGestiones(List<Gestion> gestiones) {
        this.gestiones = gestiones;
    }

    public Long getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(Long prioridad) {
        this.prioridad = prioridad;
    }

    @Override
    public String toString() {
        return "SubActividadDto{" + "id=" + id + ", nombre=" + nombre + ", modificado=" + modificado + ", actividad=" + actividad + ", gestiones=" + gestiones + '}';
    }

}
