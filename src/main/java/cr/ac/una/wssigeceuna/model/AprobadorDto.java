/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wssigeceuna.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author JosueNG
 */
public class AprobadorDto {

    private Long id;
    private String comentario;
    private String aprobacion;
    private Boolean modificado;
    private List<GestionDto> gestiones;

    public AprobadorDto() {
        this.modificado = false;
        gestiones = new ArrayList<>();
    }

    public AprobadorDto(Aprobador aprobador) {
        this();
        this.id = aprobador.getAproId();
        this.comentario = aprobador.getAproComentario();
        this.aprobacion = aprobador.getAproAprobacion();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getAprobacion() {
        return aprobacion;
    }

    public void setAprobacion(String aprobacion) {
        this.aprobacion = aprobacion;
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

    public List<GestionDto> getGestiones() {
        return gestiones;
    }

    public void setGestiones(List<GestionDto> gestiones) {
        this.gestiones = gestiones;
    }
}
