/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wssigeceuna.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 *
 * @author Lesmi
 */
@Entity
@Table(name = "SIGECE_DOCUMENTOS", schema = "SIGECEUNA")
@NamedQueries({
    @NamedQuery(name = "Documento.findAll", query = "SELECT d FROM Documento d"),
    @NamedQuery(name = "Documento.findByDocId", query = "SELECT d FROM Documento d WHERE d.id = :id")
})
public class Documento implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "SIGECE_DOCUMENTOS_DOC_ID_GENERATOR", sequenceName = "SIGECEUNA.SIGECE_DOCUMENTOS_SEQ01", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SIGECE_DOCUMENTOS_DOC_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "DOC_ID")
    private Long id;
    @Lob
    @Column(name = "DOC_ARCHIVO")
    private byte[] docArchivo;
    @Basic(optional = false)
    @Version
    @Column(name = "DOC_VERSION")
    private Long version;
    @JoinColumn(name = "DOC_IDGES", referencedColumnName = "GES_ID")
    @ManyToOne
    private Gestion gestion;

    public Documento() {
    }

    public Documento(Long docId) {
        this.id = docId;
    }

    public Documento(Long docId, byte[] docArchivo) {
        this.id = docId;
        this.docArchivo = docArchivo;
    }

    public Documento(DocumentoDto documento) {
        this.id = documento.getDocId();
        actualizarDocumento(documento);
    }

    public void actualizarDocumento(DocumentoDto documento) {
        this.docArchivo = documento.getDocArchivo();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long docId) {
        this.id = docId;
    }

    public byte[] getDocArchivo() {
        return docArchivo;
    }

    public void setDocArchivo(byte[] docArchivo) {
        this.docArchivo = docArchivo;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long docVersion) {
        this.version = docVersion;
    }

    public Gestion getGestion() {
        return gestion;
    }

    public void setGestion(Gestion gestion) {
        this.gestion = gestion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Documento)) {
            return false;
        }
        Documento other = (Documento) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.ac.una.wssigeceuna.model.Documento[ docId=" + id + " ]";
    }

}
