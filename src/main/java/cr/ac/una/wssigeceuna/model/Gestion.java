/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wssigeceuna.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 *
 * @author Lesmi
 */
@Entity
@Table(name = "SIGECE_GESTIONES", schema = "SIGECEUNA")
@NamedQueries({
    @NamedQuery(name = "Gestion.findAll", query = "SELECT g FROM Gestion g"),
    @NamedQuery(name = "Gestion.findByGesId", query = "SELECT g FROM Gestion g WHERE g.gesId = :gesId"),
    @NamedQuery(name = "Gestion.findByGesSolicitante", query = "SELECT g FROM Gestion g WHERE g.gesSolicitante = :gesSolicitante"),
    @NamedQuery(name = "Gestion.findByGesPasignada", query = "SELECT g FROM Gestion g WHERE g.gesPasignada = :gesPasignada"),
    @NamedQuery(name = "Gestion.findByGesFcreacion", query = "SELECT g FROM Gestion g WHERE g.gesFcreacion = :gesFcreacion"),
    @NamedQuery(name = "Gestion.findByGesFvencimiento", query = "SELECT g FROM Gestion g WHERE g.gesFvencimiento = :gesFvencimiento"),
    @NamedQuery(name = "Gestion.findByGesDescripcion", query = "SELECT g FROM Gestion g WHERE g.gesDescripcion = :gesDescripcion"),
    @NamedQuery(name = "Gestion.findByGesAsunto", query = "SELECT g FROM Gestion g WHERE g.gesAsunto = :gesAsunto"),
    @NamedQuery(name = "Gestion.findByGesEstado", query = "SELECT g FROM Gestion g WHERE g.gesEstado = :gesEstado")})
public class Gestion implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "SIGECE_GESTIONES_GES_ID_GENERATOR", sequenceName = "SIGECEUNA.SIGECE_GESTIONES_SEQ01", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SIGECE_GESTIONES_GES_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "GES_ID")
    private Long gesId;
    @Basic(optional = false)
    @Column(name = "GES_SOLICITANTE")
    private Long gesSolicitante;
    @Basic(optional = false)
    @Column(name = "GES_PASIGNADA")
    private Long gesPasignada;
    @Basic(optional = false)
    @Column(name = "GES_FCREACION")
    private LocalDate gesFcreacion;
    @Basic(optional = false)
    @Column(name = "GES_FVENCIMIENTO")
    private LocalDate gesFvencimiento;
    @Column(name = "GES_FSOLUCION")
    private LocalDate gesFsolucion;
    @Basic(optional = false)
    @Column(name = "GES_DESCRIPCION")
    private String gesDescripcion;
    @Basic(optional = false)
    @Column(name = "GES_ASUNTO")
    private String gesAsunto;
    @Column(name = "GES_ESTADO")
    private String gesEstado;
    @Version
    @Column(name = "GES_VERSION")
    private Long gesVersion;
    @JoinTable(name = "SIGECE_GESTIONAPROBADORES", joinColumns = {
        @JoinColumn(name = "GXA_IDGES", referencedColumnName = "GES_ID")}, inverseJoinColumns = {
        @JoinColumn(name = "GXA_IDAPRO", referencedColumnName = "APRO_ID")})
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Aprobador> aprobadores;
    @OneToMany(mappedBy = "gestion")
    private List<Documento> documentos;
    @OneToMany(mappedBy = "gestion")
    private List<Seguimiento> seguimientos;
    @JoinColumn(name = "GES_IDACT", referencedColumnName = "ACT_ID")
    @ManyToOne
    private Actividad actividad;
    @JoinColumn(name = "GES_IDSUBACT", referencedColumnName = "SUBACT_ID")
    @ManyToOne
    private Subactividad subActividad;

    public Gestion() {
    }

    public Gestion(Long gesId) {
        this.gesId = gesId;
    }

    public Gestion(Long gesId, Long gesSolicitante, Long gesPasignada, LocalDate gesFcreacion, LocalDate gesFvencimiento, String gesDescripcion, String gesAsunto) {
        this.gesId = gesId;
        this.gesSolicitante = gesSolicitante;
        this.gesPasignada = gesPasignada;
        this.gesFcreacion = gesFcreacion;
        this.gesFvencimiento = gesFvencimiento;
        this.gesDescripcion = gesDescripcion;
        this.gesAsunto = gesAsunto;
    }

    public Gestion(GestionDto gestionDto) {
        this.gesId = gestionDto.getGesId();
        actualizarGestion(gestionDto);
    }

    public void actualizarGestion(GestionDto gestionDto) {
        this.gesAsunto = gestionDto.getGesAsunto();
        this.gesDescripcion = gestionDto.getGesDescripcion();
        this.gesEstado = gestionDto.getGesEstado();
        this.gesFcreacion = gestionDto.getGesFcreacion();
        this.gesFvencimiento = gestionDto.getGesFvencimiento();
        this.gesPasignada = gestionDto.getGesPasignada();
        this.gesSolicitante = gestionDto.getGesSolicitante();
        this.gesFsolucion = gestionDto.getGesFsolucion();
    }

    public Long getGesId() {
        return gesId;
    }

    public void setGesId(Long gesId) {
        this.gesId = gesId;
    }

    public Long getGesSolicitante() {
        return gesSolicitante;
    }

    public void setGesSolicitante(Long gesSolicitante) {
        this.gesSolicitante = gesSolicitante;
    }

    public Long getGesPasignada() {
        return gesPasignada;
    }

    public void setGesPasignada(Long gesPasignada) {
        this.gesPasignada = gesPasignada;
    }

    public LocalDate getGesFcreacion() {
        return gesFcreacion;
    }

    public void setGesFcreacion(LocalDate gesFcreacion) {
        this.gesFcreacion = gesFcreacion;
    }

    public LocalDate getGesFvencimiento() {
        return gesFvencimiento;
    }

    public void setGesFvencimiento(LocalDate gesFvencimiento) {
        this.gesFvencimiento = gesFvencimiento;
    }

    public String getGesDescripcion() {
        return gesDescripcion;
    }

    public void setGesDescripcion(String gesDescripcion) {
        this.gesDescripcion = gesDescripcion;
    }

    public String getGesAsunto() {
        return gesAsunto;
    }

    public void setGesAsunto(String gesAsunto) {
        this.gesAsunto = gesAsunto;
    }

    public String getGesEstado() {
        return gesEstado;
    }

    public void setGesEstado(String gesEstado) {
        this.gesEstado = gesEstado;
    }

    public Long getGesVersion() {
        return gesVersion;
    }

    public void setGesVersion(Long gesVersion) {
        this.gesVersion = gesVersion;
    }

    public List<Aprobador> getAprobadores() {
        return aprobadores;
    }

    public void setAprobadores(List<Aprobador> aprobadores) {
        this.aprobadores = aprobadores;
    }

    public List<Documento> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<Documento> documentos) {
        this.documentos = documentos;
    }

    public List<Seguimiento> getSeguimientos() {
        return seguimientos;
    }

    public void setSeguimientos(List<Seguimiento> seguimientos) {
        this.seguimientos = seguimientos;
    }

    public Actividad getActividad() {
        return actividad;
    }

    public void setActividad(Actividad actividad) {
        this.actividad = actividad;
    }

    public Subactividad getSubActividad() {
        return subActividad;
    }

    public void setSubActividad(Subactividad subActividad) {
        this.subActividad = subActividad;
    }
        public LocalDate getGesFsolucion() {
        return gesFsolucion;
    }

    public void setGesFsolucion(LocalDate gesFsolucion) {
        this.gesFsolucion = gesFsolucion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gesId != null ? gesId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Gestion)) {
            return false;
        }
        Gestion other = (Gestion) object;
        if ((this.gesId == null && other.gesId != null) || (this.gesId != null && !this.gesId.equals(other.gesId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.ac.una.wssigeceuna.model.Gestion[ gesId=" + gesId + " ]";
    }

}
