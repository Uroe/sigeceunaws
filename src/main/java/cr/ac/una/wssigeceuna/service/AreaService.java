/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wssigeceuna.service;

import cr.ac.una.wssigeceuna.model.Actividad;
import cr.ac.una.wssigeceuna.model.ActividadDto;
import cr.ac.una.wssigeceuna.model.Area;
import cr.ac.una.wssigeceuna.model.AreaDto;
import cr.ac.una.wssigeceuna.model.Empleado;
import cr.ac.una.wssigeceuna.model.EmpleadoDto;
import cr.ac.una.wssigeceuna.util.CodigoRespuesta;
import cr.ac.una.wssigeceuna.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author JosueNG
 */
@Stateless
@LocalBean
public class AreaService {

    private static final Logger LOG = Logger.getLogger(AreaService.class.getName());
    @PersistenceContext(unitName = "WsSigeceUNAPU")
    private EntityManager em;

    public Respuesta getArea(Long id) {
        try {
            Query qryArea = em.createNamedQuery("Area.findByAreId", Area.class);
            qryArea.setParameter("areId", id);

            Area area = (Area) qryArea.getSingleResult();
            AreaDto areaDto = new AreaDto(area);

            for (Actividad actividade : area.getActividades()) {
                areaDto.getActividades().add(new ActividadDto(actividade));
            }
            for (Empleado empleado : area.getEmpleados()) {
                areaDto.getEmpleados().add(new EmpleadoDto(empleado));
            }

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Area", areaDto);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existe un area con el código ingresado.", "getArea NoResultException");
        } catch (NonUniqueResultException ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar la area.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar la area.", "getArea NonUniqueResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar la area.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar la area.", "getArea " + ex.getMessage());
        }
    }

    public Respuesta getAreas() {
        try {
            Query qryAreas = em.createNamedQuery("Area.findAll", Area.class);
            List<Area> areas = qryAreas.getResultList();
            List<AreaDto> areasDto = new ArrayList<>();
            for (Area area : areas) {
                areasDto.add(new AreaDto(area));
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Areas", areasDto);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Error consultando las areas.", "getAreas NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar las areas.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar las areas.", "getAreas " + ex.getMessage());
        }
    }

    public Respuesta guardarArea(AreaDto areaDto) {
        try {
            Area area;
            if (areaDto.getId() != null && areaDto.getId() > 0) {
                area = em.find(Area.class, areaDto.getId());
                if (area == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró la area a modificar.", "guardarArea NoResultException");
                }
                area.actualizarArea(areaDto);
                for (EmpleadoDto emp : areaDto.getEmpleadosEliminados()) {
                    area.getEmpleados().remove(new Empleado(emp.getId()));
                }
                if (!areaDto.getEmpleados().isEmpty()) {
                    for (EmpleadoDto emp : areaDto.getEmpleados()) {
                        if (emp.getModificado()) {
                            Empleado empleado = em.find(Empleado.class, emp.getId());
                            empleado.getAreas().add(area);
                            area.getEmpleados().add(empleado);
                        }
                    }
                }
                area = em.merge(area);
            } else {
                area = new Area(areaDto);
                em.persist(area);
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Area", new AreaDto(area));
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar la area .", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al guardar la area.", "guardarArea " + ex.getMessage());
        }
    }

    public Respuesta eliminarArea(Long id) {
        try {
            Area area;
            if (id != null && id > 0) {
                area = em.find(Area.class, id);
                if (area == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró el area a eliminar.", "eliminarArea NoResultException");
                }
                em.remove(area);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar el area a eliminar.", "eliminarArea NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        } catch (Exception ex) {
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar el area porque tiene relaciones con otros registros.", "eliminarArea " + ex.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al eliminar el area.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar el area.", "eliminarArea " + ex.getMessage());
        }
    }
}
