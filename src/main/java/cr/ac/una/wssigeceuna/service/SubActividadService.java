/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wssigeceuna.service;

import cr.ac.una.wssigeceuna.model.Actividad;
import cr.ac.una.wssigeceuna.model.ActividadDto;
import cr.ac.una.wssigeceuna.model.Area;
import cr.ac.una.wssigeceuna.model.Subactividad;
import cr.ac.una.wssigeceuna.model.SubActividadDto;
import cr.ac.una.wssigeceuna.util.CodigoRespuesta;
import cr.ac.una.wssigeceuna.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Ale
 */
@Stateless
@LocalBean
public class SubActividadService {
    
    private static final Logger LOG = Logger.getLogger(SubActividadService.class.getName());
    @PersistenceContext(unitName = "WsSigeceUNAPU")
    private EntityManager em;
    
    public Respuesta getSubActividad(Long id) {
        try {
            Query qrySubActividad = em.createNamedQuery("Subactividad.findBySubactId", Subactividad.class);
            qrySubActividad.setParameter("id", id);
            
            Subactividad subactividad = (Subactividad) qrySubActividad.getSingleResult();
            SubActividadDto subActividadDto = new SubActividadDto(subactividad);
            
            subActividadDto.setActividad(new ActividadDto(subactividad.getActividad()));
            
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "SubActividad", subActividadDto);
            
        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existe una subactividad con el código ingresado.", "getSubActividad NoResultException");
        } catch (NonUniqueResultException ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar la subactividad.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar la subactividad.", "getSubActividad NonUniqueResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar la subactividad.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar la subactividad.", "getSubActividad " + ex.getMessage());
        }
    }
    
    public Respuesta getSubActividades() {
        try {
            Query qrySubActividades = em.createNamedQuery("Subactividad.findAll", Actividad.class);
            List<Subactividad> subActividades = qrySubActividades.getResultList();
            
            List<SubActividadDto> subActividadDtos = new ArrayList<>();
            for (Subactividad subActividad : subActividades) {
                subActividadDtos.add(new SubActividadDto(subActividad));
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "SubActividades", subActividadDtos);
            
        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Error consultando las subactividades.", "getSubActividades NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar las subactividades.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar las subactividades.", "getSubActividades " + ex.getMessage());
        }
    }
    
    public Respuesta cambiarPrioridad(Long id, Long prioridad) {
        try {
            Query qrySubActividad = em.createNamedQuery("Subactividad.findBySubactId", Subactividad.class);
            qrySubActividad.setParameter("id", id);
            
            Subactividad subActividad = (Subactividad) qrySubActividad.getSingleResult();
            
            if (subActividad == null) {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró la actividad a modificar.", "cambiarPrioridad NoResultException");
            } else {
                subActividad.setPrioridad(prioridad);
                subActividad = em.merge(subActividad);
                em.flush();
                return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "SubActividad", new SubActividadDto(subActividad));
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al cambiar la prioridad.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al cambiar la prioridad.", "cambiarPrioridad " + ex.getMessage());
        }
    }
    
    public Respuesta guardarSubActividad(SubActividadDto subActividadDto) {
        try {
            Actividad actividad = em.find(Actividad.class, subActividadDto.getActividad().getId());
            if (actividad == null) {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró un actividad relacionada a esta subActividad", "guardarSubActividad NoResultException");
            } else {
                Subactividad subActividad;
                if (subActividadDto.getId() != null && subActividadDto.getId() > 0) {
                    subActividad = em.find(Subactividad.class, subActividadDto.getId());
                    if (subActividad == null) {
                        return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró la subactividad a modificar.", "guardarSubActividad NoResultException");
                    }
                    subActividad.actualizarSubActividad(subActividadDto);
                    subActividad.setActividad(actividad);
                    subActividad = em.merge(subActividad);
                } else {
                    subActividad = new Subactividad(subActividadDto);
                    subActividad.setActividad(actividad);
                    em.persist(subActividad);
                }
                em.flush();
                return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "SubActividad", new SubActividadDto(subActividad));
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar la subactividad .", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al guardar la subactividad.", "guardarSubActividad " + ex.getMessage());
        }
    }
    
    public Respuesta eliminarSubActividad(Long id) {
        try {
            Subactividad subActividad;
            if (id != null && id > 0) {
                subActividad = em.find(Subactividad.class, id);
                if (subActividad == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró la subactividad a eliminar.", "eliminarSubActividad NoResultException");
                }
                em.remove(subActividad);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar la subactividad a eliminar.", "eliminarSubActividad NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        } catch (Exception ex) {
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar la subactividad porque tiene relaciones con otros registros.", "eliminarSubActividad " + ex.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al eliminar el area.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar la subactividad.", "eliminarSubActividad " + ex.getMessage());
        }
    }
    
}
