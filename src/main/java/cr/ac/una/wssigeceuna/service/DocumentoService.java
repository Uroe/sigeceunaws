/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wssigeceuna.service;

import cr.ac.una.wssigeceuna.model.Documento;
import cr.ac.una.wssigeceuna.model.DocumentoDto;
import cr.ac.una.wssigeceuna.model.Gestion;
import cr.ac.una.wssigeceuna.util.CodigoRespuesta;
import cr.ac.una.wssigeceuna.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Ale
 */
@Stateless
@LocalBean
public class DocumentoService {

    private static final Logger LOG = Logger.getLogger(DocumentoService.class.getName());
    @PersistenceContext(unitName = "WsSigeceUNAPU")
    private EntityManager em;

    public Respuesta getDocumento(Long id) {
        try {
            Query qryDocumento = em.createNamedQuery("Documento.findByDocId", Documento.class);
            qryDocumento.setParameter("id", id);

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Documento", new DocumentoDto((Documento) qryDocumento.getSingleResult()));

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existe un documento con el código ingresado.", "getDocumento NoResultException");
        } catch (NonUniqueResultException ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el documento.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el documento.", "getDocumento NonUniqueResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el documento.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el documento.", "getDocumento " + ex.getMessage());
        }
    }

    public Respuesta guardarDocumento(DocumentoDto documentoDto) {
        try {
            Gestion gestion = em.find(Gestion.class, documentoDto.getGestion().getGesId());
            if (gestion == null) {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró una gestion relacionada a esta actividad", "guardarDocumento NoResultException");
            } else {
                Documento documento;
                if (documentoDto.getDocId() != null && documentoDto.getDocId() > 0) {
                    documento = em.find(Documento.class, documentoDto.getDocId());
                    if (documento == null) {
                        return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró el documento a modificar.", "guardarDocumento NoResultException");
                    }
                    documento.actualizarDocumento(documentoDto);
                    documento = em.merge(documento);
                } else {
                    documento = new Documento(documentoDto);
                    documento.setGestion(gestion);
                    em.persist(documento);
                }
                em.flush();
                return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Documento", new DocumentoDto(documento));
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el documento.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al guardar el documento.", "guardarDocumento " + ex.getMessage());
        }
    }

    public Respuesta eliminarDocumento(Long id) {
        try {
            Documento documento;
            if (id != null && id > 0) {
                documento = em.find(Documento.class, id);
                if (documento == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró el documento a eliminar.", "eliminarDocumento NoResultException");
                }
                em.remove(documento);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar el documento a eliminar.", "eliminarDocumento NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        } catch (Exception ex) {
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar el documento porque tiene relaciones con otros registros.", "eliminarDocumento" + ex.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al eliminar el documento.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar el documento.", "eliminarDocumento" + ex.getMessage());
        }
    }

}
