/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wssigeceuna.service;

import cr.ac.una.wssigeceuna.model.Actividad;
import cr.ac.una.wssigeceuna.model.ActividadDto;
import cr.ac.una.wssigeceuna.model.Aprobador;
import cr.ac.una.wssigeceuna.model.AprobadorDto;
import cr.ac.una.wssigeceuna.model.Documento;
import cr.ac.una.wssigeceuna.model.DocumentoDto;
import cr.ac.una.wssigeceuna.model.Gestion;
import cr.ac.una.wssigeceuna.model.GestionDto;
import cr.ac.una.wssigeceuna.model.Seguimiento;
import cr.ac.una.wssigeceuna.model.SeguimientoDto;
import cr.ac.una.wssigeceuna.model.SubActividadDto;
import cr.ac.una.wssigeceuna.model.Subactividad;
import cr.ac.una.wssigeceuna.util.CodigoRespuesta;
import cr.ac.una.wssigeceuna.util.Respuesta;
import java.io.InputStream;
import java.sql.SQLIntegrityConstraintViolationException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

/**
 *
 * @author Lesmi
 */
@Stateless
@LocalBean
public class GestionService {

    private static final Logger LOG = Logger.getLogger(AreaService.class.getName());
    @PersistenceContext(unitName = "WsSigeceUNAPU")
    private EntityManager em;

    public Respuesta getGestion(Long id) {
        try {
            Query qryGestion = em.createNamedQuery("Gestion.findByGesId", Gestion.class);
            qryGestion.setParameter("gesId", id);

            Gestion gestion = (Gestion) qryGestion.getSingleResult();
            GestionDto gestionDto = new GestionDto(gestion);

            for (Aprobador aprobador : gestion.getAprobadores()) {
                gestionDto.getAprobadores().add(new AprobadorDto(aprobador));
            }

            for (Seguimiento seguimiento : gestion.getSeguimientos()) {
                gestionDto.getSeguimientos().add(new SeguimientoDto(seguimiento));
            }

            for (Documento documento : gestion.getDocumentos()) {
                gestionDto.getDocumentos().add(new DocumentoDto(documento));
            }
            if (gestion.getActividad() != null) {
                gestionDto.setActividad(new ActividadDto(gestion.getActividad()));
            }
            if (gestion.getSubActividad() != null) {
                gestionDto.setSubActividad(new SubActividadDto(gestion.getSubActividad()));
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Gestion", gestionDto);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existe una gestion con el código ingresado.", "getGestion NoResultException");
        } catch (NonUniqueResultException ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar la gestion.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar la gestion.", "getGestion NonUniqueResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar la gestion.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar la gestion.", "getGestion " + ex.getMessage());
        }
    }

    public Respuesta getGestiones() {
        try {
            Query qryGestion = em.createNamedQuery("Gestion.findAll", Gestion.class);
            List<Gestion> gestiones = qryGestion.getResultList();

            List<GestionDto> gestionDtos = new ArrayList<>();
            for (Gestion gestion : gestiones) {
                gestionDtos.add(new GestionDto(gestion));
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Gestiones", gestionDtos);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen gestiones con los criterios ingresados.", "buscarGestiones NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar las gestiones.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar las gestiones.", "buscarGestiones " + ex.getMessage());
        }
    }

    public Respuesta getGestionesMes(int mes) {
        try {
            Query qryGestion = em.createNamedQuery("Gestion.findAll", Gestion.class);
            List<Gestion> gestiones = qryGestion.getResultList();

            List<GestionDto> gestionDtos = new ArrayList<>();
            for (Gestion gestion : gestiones) {
                if (gestion.getGesFvencimiento().getMonth().equals(mes)) {
                    gestionDtos.add(new GestionDto(gestion));
                }
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "GestionesMes", gestionDtos);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen gestiones con los criterios ingresados.", "buscarGestiones NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar las gestiones.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar las gestiones.", "buscarGestiones " + ex.getMessage());
        }
    }

    public Respuesta getGestionesSolicitadas(Long idSoli) {
        try {
            Query qryGestion = em.createNamedQuery("Gestion.findByGesSolicitante", Gestion.class);
            qryGestion.setParameter("gesSolicitante", idSoli);
            List<Gestion> gestiones = qryGestion.getResultList();

            List<GestionDto> gestionDtos = new ArrayList<>();
            for (Gestion gestion : gestiones) {
                gestionDtos.add(new GestionDto(gestion));
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Gestiones", gestionDtos);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen gestiones con los criterios ingresados.", "buscarGestiones NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar las gestiones.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar las gestiones.", "buscarGestiones " + ex.getMessage());
        }
    }

    public Respuesta getGestionesSoliFin(Long idSoli) {
        try {
            Query qryGestion = em.createNamedQuery("Gestion.findByGesSolicitante", Gestion.class);
            qryGestion.setParameter("gesSolicitante", idSoli);
            List<Gestion> gestiones = qryGestion.getResultList();

            List<Gestion> filtro = gestiones.stream().filter(g -> g.getGesEstado().equals("S") || g.getGesEstado().equals("A")
                    || g.getGesFvencimiento().isBefore(LocalDate.now())).collect(Collectors.toList());
            List<GestionDto> gestionDtos = new ArrayList<>();

            for (Gestion gestion : filtro) {
                gestionDtos.add(new GestionDto(gestion));
            }

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Gestiones", gestionDtos);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen gestiones con los criterios ingresados.", "buscarGestiones NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar las gestiones.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar las gestiones.", "buscarGestiones " + ex.getMessage());
        }
    }

    public Respuesta getGestionesAsignadas(Long idAsig) {
        try {
            Query qryGestion = em.createNamedQuery("Gestion.findByGesPasignada", Gestion.class);
            qryGestion.setParameter("gesPasignada", idAsig);
            List<Gestion> gestiones = qryGestion.getResultList();

            List<Gestion> filtro = gestiones.stream().filter(g -> g.getGesEstado().equals("P") || g.getGesEstado().equals("C")
                    || g.getGesEstado().equals("E") || g.getGesEstado().equals("R")).collect(Collectors.toList());

            List<GestionDto> gestionDtos = new ArrayList<>();
            for (Gestion gestion : filtro) {
                gestionDtos.add(new GestionDto(gestion));
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Gestiones", gestionDtos);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen gestiones con los criterios ingresados.", "buscarGestiones NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar las gestiones.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar las gestiones.", "buscarGestiones " + ex.getMessage());
        }
    }

    public Respuesta guardarGestion(GestionDto gestionDto) {
        try {
            Actividad actividad = em.find(Actividad.class, gestionDto.getActividad().getId());
            if (actividad == null) {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró una actividad relacionada a esta gestion", "guardarGestion NoResultException");
            } else {
                Gestion gestion;
                if (gestionDto.getGesId() != null && gestionDto.getGesId() > 0) {
                    gestion = em.find(Gestion.class, gestionDto.getGesId());
                    if (gestion == null) {
                        return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró la gestion a modificar.", "guardarGestion NoResultException");
                    }
                    gestion.actualizarGestion(gestionDto);
                    for (AprobadorDto apro : gestionDto.getAprobadoresElim()) {
                        gestion.getAprobadores().remove(new Aprobador(apro.getId()));
                    }
                    gestion.setActividad(actividad);
                    if (gestionDto.getSubActividad().getNombre() != null) {
                        Subactividad subactividad = em.find(Subactividad.class, gestionDto.getSubActividad().getId());
                        gestion.setSubActividad(subactividad);
                    }
                    if (!gestionDto.getAprobadores().isEmpty()) {
                        for (AprobadorDto apro : gestionDto.getAprobadores()) {
                            if (apro.getModificado()) {
                                Aprobador aprobador = em.find(Aprobador.class, apro.getId());
                                aprobador.getGestiones().add(gestion);
                                gestion.getAprobadores().add(aprobador);
                            }
                        }
                    }
                    gestion = em.merge(gestion);
                } else {
                    gestion = new Gestion(gestionDto);
                    gestion.setActividad(actividad);
                    if (gestionDto.getSubActividad().getNombre() != null) {
                        Subactividad subactividad = em.find(Subactividad.class, gestionDto.getSubActividad().getId());
                        gestion.setSubActividad(subactividad);
                    }
                    em.persist(gestion);
                    if (!gestionDto.getAprobadores().isEmpty()) {
                        for (AprobadorDto apro : gestionDto.getAprobadores()) {
                            if (apro.getModificado()) {
                                Aprobador aprobador = em.find(Aprobador.class, apro.getId());
                                aprobador.getGestiones().add(gestion);
                                gestion.getAprobadores().add(aprobador);
                            }
                        }
                    }
                    gestion = em.merge(gestion);
                }
                em.flush();
                return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Gestion", new GestionDto(gestion));
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar la gestion .", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al guardar la gestion.", "guardarGestion " + ex.getMessage());
        }
    }

    public Respuesta cambiarEstado(Long id, String estado) {
        try {
            Query qryGestion = em.createNamedQuery("Gestion.findByGesId", Gestion.class);
            qryGestion.setParameter("gesId", id);

            Gestion gestion = (Gestion) qryGestion.getSingleResult();

            if (gestion == null) {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró la gestion a modificar.", "cambiarEstado NoResultException");
            } else {
                gestion.setGesEstado(estado);
                gestion = em.merge(gestion);
                em.flush();
                return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Gestion", new GestionDto(gestion));
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al cambiar el estado de la solucion.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al cambiar el estado de la solucion.", "cambiarEstado " + ex.getMessage());
        }
    }

    public Respuesta asignarFsolucion(Long id) {
        try {
            Query qryGestion = em.createNamedQuery("Gestion.findByGesId", Gestion.class);
            qryGestion.setParameter("gesId", id);

            Gestion gestion = (Gestion) qryGestion.getSingleResult();

            if (gestion == null) {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró la gestion a modificar.", "asignarFsolucion NoResultException");
            } else {
                gestion.setGesFsolucion(LocalDate.now());
                gestion = em.merge(gestion);
                em.flush();
                return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Gestion", new GestionDto(gestion));
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al asignar la fecha.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al asignar la fecha.", "asignarFsolucion " + ex.getMessage());
        }
    }

    public Respuesta eliminarGestion(Long id) {
        try {
            Gestion gestion;
            if (id != null && id > 0) {
                gestion = em.find(Gestion.class, id);
                if (gestion == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encoontró la gestion a eliminar.", "eliminarGestion NoResultException");
                }
                em.remove(gestion);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar la gestion a eliminar.", "eliminarGestion NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        } catch (Exception ex) {
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar la gestion porque tiene relaciones con otros registros.", "eliminarGestion " + ex.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al eliminar la gestion.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar la gestion.", "eliminarGestion " + ex.getMessage());
        }
    }

    public Respuesta getReporte(Long id, String fecha1, String fecha2) {
        try {
            java.sql.Connection connection = em.unwrap(java.sql.Connection.class);
            InputStream stream = GestionService.class.getClassLoader().
                    getResourceAsStream("jasper/gestionesxempleado.jasper");
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("id", id);
            map.put("fecha1", fecha1);
            map.put("fecha2", fecha2);
            JasperPrint prin = JasperFillManager.fillReport(stream, map, connection);
            String xml = JasperExportManager.exportReportToXml(prin);

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Reporte", xml);
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al obtener el reporte.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al obtener el reporte.", "getReporte " + ex.getMessage());
        }
    }

    public Respuesta getReporteAreas() {
        try {
            java.sql.Connection connection = em.unwrap(java.sql.Connection.class);
            InputStream stream = GestionService.class.getClassLoader().
                    getResourceAsStream("jasper/areas.jasper");
            JasperPrint prin = JasperFillManager.fillReport(stream, null, connection);
            String xml = JasperExportManager.exportReportToXml(prin);

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Reporte", xml);
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al obtener el reporte.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al obtener el reporte.", "getReporteAreas " + ex.getMessage());
        }
    }

    public Respuesta getReporteEmpleados(Long id) {
        try {
            java.sql.Connection connection = em.unwrap(java.sql.Connection.class);
            InputStream stream = GestionService.class.getClassLoader().
                    getResourceAsStream("jasper/empleadosxarea.jasper");
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("id", id);
            JasperPrint prin = JasperFillManager.fillReport(stream, map, connection);
            String xml = JasperExportManager.exportReportToXml(prin);

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Reporte", xml);
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al obtener el reporte.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al obtener el reporte.", "getReporteEmpleados " + ex.getMessage());
        }
    }

}
