/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wssigeceuna.service;

import cr.ac.una.wssigeceuna.model.Gestion;
import cr.ac.una.wssigeceuna.model.GestionDto;
import cr.ac.una.wssigeceuna.model.Seguimiento;
import cr.ac.una.wssigeceuna.model.SeguimientoDto;
import cr.ac.una.wssigeceuna.util.CodigoRespuesta;
import cr.ac.una.wssigeceuna.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Ale
 */
@Stateless
@LocalBean
public class SeguimientoService {

    private static final Logger LOG = Logger.getLogger(SeguimientoService.class.getName());
    @PersistenceContext(unitName = "WsSigeceUNAPU")
    private EntityManager em;

    public Respuesta getSeguimiento(Long id) {
        try {
            Query qrySeguimiento = em.createNamedQuery("Seguimiento.findBySegId", Seguimiento.class);
            qrySeguimiento.setParameter("id", id);

            Seguimiento seguimiento = (Seguimiento) qrySeguimiento.getSingleResult();
            SeguimientoDto seguimientoDto = new SeguimientoDto(seguimiento);

            seguimientoDto.setGestion(new GestionDto(seguimiento.getGestion()));

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Seguimiento", seguimientoDto);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existe un seguimiento con el código ingresado.", "getSeguimiento NoResultException");
        } catch (NonUniqueResultException ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el seguimiento.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el seguimiento.", "getSeguimiento NonUniqueResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el seguimiento.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el seguimiento.", "getSeguimiento " + ex.getMessage());
        }
    }

    public Respuesta getSeguimientos(Long id) {
        try {
            Query qrySeguimiento = em.createNamedQuery("Seguimiento.findAll", Seguimiento.class);
            List<Seguimiento> seguimientos = qrySeguimiento.getResultList();

            List<Seguimiento> segxGes = seguimientos.stream().filter(g -> g.getGestion().getGesId().equals(id)).collect(
                    Collectors.toList());

            List<SeguimientoDto> seguimientoDtos = new ArrayList<>();
            for (Seguimiento seg : segxGes) {
                seguimientoDtos.add(new SeguimientoDto(seg));
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Seguimientos", seguimientoDtos);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen seguimientos con los criterios ingresados.", "getSeguimientos NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar los seguimientos.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar los seguimientos.", "getSeguimientos " + ex.getMessage());
        }
    }

    public Respuesta guardarSeguimiento(SeguimientoDto seguimientoDto) {
        try {
            Gestion gestion = em.find(Gestion.class, seguimientoDto.getGestion().getGesId());
            if (gestion == null) {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró una gestion relacionada a este seguimiento", "guardarSeguimiento NoResultException");
            } else {
                Seguimiento seguimiento;
                if (seguimientoDto.getSegId() != null && seguimientoDto.getSegId() > 0) {
                    seguimiento = em.find(Seguimiento.class, seguimientoDto.getSegId());
                    if (seguimiento == null) {
                        return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró el seguimiento a modificar.", "guardarSeguimiento NoResultException");
                    }
                    seguimiento.actualizarSeguimiento(seguimientoDto);
                    seguimiento.setGestion(gestion);
                    seguimiento = em.merge(seguimiento);
                } else {
                    seguimiento = new Seguimiento(seguimientoDto);
                    seguimiento.setGestion(gestion);
                    em.persist(seguimiento);
                }
                em.flush();
                return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Seguimiento", new SeguimientoDto(seguimiento));
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el seguimiento.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al guardar el seguimiento.", "guardarSeguimiento " + ex.getMessage());
        }
    }

    public Respuesta eliminarSeguimiento(Long id) {
        try {
            Seguimiento seguimiento;
            if (id != null && id > 0) {
                seguimiento = em.find(Seguimiento.class, id);
                if (seguimiento == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró el seguimiento a eliminar.", "eliminarSeguimiento NoResultException");
                }
                em.remove(seguimiento);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar el seguimiento a eliminar.", "eliminarSeguimiento NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        } catch (Exception ex) {
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar el seguimiento porque tiene relaciones con otros registros.", "eliminarSeguimiento " + ex.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al eliminar el seguimiento.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar el seguimiento.", "eliminarSeguimiento " + ex.getMessage());
        }
    }

}
