/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wssigeceuna.service;

import cr.ac.una.wssigeceuna.model.Actividad;
import cr.ac.una.wssigeceuna.model.ActividadDto;
import cr.ac.una.wssigeceuna.model.Area;
import cr.ac.una.wssigeceuna.model.AreaDto;
import cr.ac.una.wssigeceuna.model.SubActividadDto;
import cr.ac.una.wssigeceuna.model.Subactividad;
import cr.ac.una.wssigeceuna.util.CodigoRespuesta;
import cr.ac.una.wssigeceuna.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author JosueNG
 */
@Stateless
@LocalBean
public class ActividadService {
    
    private static final Logger LOG = Logger.getLogger(ActividadService.class.getName());
    @PersistenceContext(unitName = "WsSigeceUNAPU")
    private EntityManager em;
    
    public Respuesta getActividad(Long id) {
        try {
            Query qryActividad = em.createNamedQuery("Actividad.findByActId", Actividad.class);
            qryActividad.setParameter("actId", id);
            Actividad actividad = (Actividad) qryActividad.getSingleResult();
            ActividadDto actividadDto = new ActividadDto(actividad);
            
            actividadDto.setArea(new AreaDto(actividad.getArea()));
            
            for (Subactividad subActividade : actividad.getSubActividades()) {
                actividadDto.getSubactividades().add(new SubActividadDto(subActividade));
            }
            
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Actividad", actividadDto);
            
        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existe una actividad con el código ingresado.", "getActividad NoResultException");
        } catch (NonUniqueResultException ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar la actividad.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar la actividad.", "getActividad NonUniqueResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar la actividad.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar la actividad.", "getActividad " + ex.getMessage());
        }
    }
    
    public Respuesta getActividades() {
        try {
            Query qryActividades = em.createNamedQuery("Actividad.findAll", Actividad.class);
            List<Actividad> actividades = qryActividades.getResultList();
            
            List<ActividadDto> actividadDtos = new ArrayList<>();
            for (Actividad actividad : actividades) {
                actividadDtos.add(new ActividadDto(actividad));
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Actividades", actividadDtos);
            
        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Error consultando las actividades.", "getActividades NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar las actividades.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar las actividades.", "getActividades " + ex.getMessage());
        }
    }
    
    public Respuesta cambiarPrioridad(Long id, Long prioridad) {
        try {
            Query qryActividad = em.createNamedQuery("Actividad.findByActId", Actividad.class);
            qryActividad.setParameter("actId", id);
            
            Actividad actividad = (Actividad) qryActividad.getSingleResult();
            
            if (actividad == null) {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró la actividad a modificar.", "cambiarPrioridad NoResultException");
            } else {
                actividad.setActPrioridad(prioridad);
                actividad = em.merge(actividad);
                em.flush();
                return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Actividad", new ActividadDto(actividad));
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al cambiar la prioridad.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al cambiar la prioridad.", "cambiarPrioridad " + ex.getMessage());
        }
    }
    
    public Respuesta guardarActividad(ActividadDto actividadDto) {
        try {
            Area area = em.find(Area.class, actividadDto.getArea().getId());
            if (area == null) {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró un area relacionada a esta actividad", "guardarActividad NoResultException");
            } else {
                Actividad actividad;
                if (actividadDto.getId() != null && actividadDto.getId() > 0) {
                    actividad = em.find(Actividad.class, actividadDto.getId());
                    if (actividad == null) {
                        return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró la actividad a modificar.", "guardarActividad NoResultException");
                    }
                    actividad.actualizarActividad(actividadDto);
                    actividad.setArea(area);
                    actividad = em.merge(actividad);
                } else {
                    actividad = new Actividad(actividadDto);
                    actividad.setArea(area);
                    em.persist(actividad);
                }
                em.flush();
                return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Actividad", new ActividadDto(actividad));
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar la actividad.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al guardar la actividad.", "guardarActividad " + ex.getMessage());
        }
    }
    
    public Respuesta eliminarActividad(Long id) {
        try {
            Actividad actividad;
            if (id != null && id > 0) {
                actividad = em.find(Actividad.class, id);
                if (actividad == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró la actividad a eliminar.", "eliminarActividad NoResultException");
                }
                em.remove(actividad);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar la actividad a eliminar.", "eliminarActividad NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        } catch (Exception ex) {
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar la actividad porque tiene relaciones con otros registros.", "eliminarActividad " + ex.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al eliminar la actividad.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar la actividad.", "eliminarActividad " + ex.getMessage());
        }
    }
}
