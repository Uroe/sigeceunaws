/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.wssigeceuna.service;

import cr.ac.una.wssigeceuna.model.Aprobador;
import cr.ac.una.wssigeceuna.model.AprobadorDto;
import cr.ac.una.wssigeceuna.model.Gestion;
import cr.ac.una.wssigeceuna.model.GestionDto;
import cr.ac.una.wssigeceuna.util.CodigoRespuesta;
import cr.ac.una.wssigeceuna.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author JosueNG
 */
@Stateless
@LocalBean
public class AprobadorService {

    private static final Logger LOG = Logger.getLogger(AprobadorService.class.getName());
    @PersistenceContext(unitName = "WsSigeceUNAPU")
    private EntityManager em;

    public Respuesta getAprobador(Long id) {
        try {
            Query qryAprobador = em.createNamedQuery("Aprobador.findByAproId", Aprobador.class);
            qryAprobador.setParameter("aproId", id);

            Aprobador apro = (Aprobador) qryAprobador.getSingleResult();
            AprobadorDto aprobadorDto = new AprobadorDto(apro);

            for (Gestion gestione : apro.getGestiones()) {
                aprobadorDto.getGestiones().add(new GestionDto(gestione));
            }

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Aprobador", aprobadorDto);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existe un aprobador con el código ingresado.", "getAprobador NoResultException");
        } catch (NonUniqueResultException ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el aprobador.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el aprobador.", "getAprobador NonUniqueResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el aprobador.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el aprobador.", "getAprobador " + ex.getMessage());
        }
    }

    public Respuesta getAprobadores() {
        try {
            Query qryAprobador = em.createNamedQuery("Aprobador.findAll", Aprobador.class);
            List<Aprobador> aprobadores = qryAprobador.getResultList();

            List<AprobadorDto> aprobadorDtos = new ArrayList<>();
            for (Aprobador apro : aprobadores) {
                aprobadorDtos.add(new AprobadorDto(apro));
            }

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Aprobadores", aprobadorDtos);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen aprobadores con los criterios ingresados.", "getAprobadores NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar los aprobadores.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar los aprobadores.", "getAprobadores " + ex.getMessage());
        }
    }

    public Respuesta guardarAprobador(AprobadorDto aprobadorDto) {
        try {
            Aprobador aprobador;
            aprobador = em.find(Aprobador.class, aprobadorDto.getId());
            if (aprobador == null) {
                aprobador = new Aprobador(aprobadorDto);
                em.persist(aprobador);
            } else {
                aprobador.actualizarAprobador(aprobadorDto);
                aprobador = em.merge(aprobador);
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Aprobador", new AprobadorDto(aprobador));
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el aprobador .", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al guardar el aprobador.", "guardarAprobador " + ex.getMessage());
        }
    }

    public Respuesta eliminarAprobador(Long id) {
        try {
            Aprobador aprobador;
            if (id != null && id > 0) {
                aprobador = em.find(Aprobador.class, id);
                if (aprobador == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró el aprobador a eliminar.", "eliminarAprobador NoResultException");
                }
                em.remove(aprobador);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar el aprobador a eliminar.", "eliminarAprobador NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        } catch (Exception ex) {
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar el aprobador porque tiene relaciones con otros registros.", "eliminarAprobador " + ex.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al eliminar el aprobador.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar el aprobador.", "eliminarAprobador " + ex.getMessage());
        }
    }
}
